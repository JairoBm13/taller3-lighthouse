'use strict';

const Audit = require('lighthouse').Audit;

const MAX_API_RESPONSE = 3000;

class ApiAudit extends Audit {
    static get meta() {
        return {
            category: 'MyPerformance',
            name: 'api-audit',
            description: 'api response ready ',
            failureDescription: 'API response slow to get',
            helpText: 'Used to measure time from navigationStart to when the schedule' +
            ' api response is ready',

            requiredArtifacts: ['TimeToApi']
        };
    }

    static audit(artifacts) {
        const loadedTime = artifacts.TimeToApi;

        const belowThreshold = loadedTime <= MAX_API_RESPONSE;

        return {
            rawValue: loadedTime,
            score: belowThreshold
        };
    }
}

module.exports = ApiAudit;